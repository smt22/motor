// Grossly verbose

#include "../include/motor.h"

int main(int argc, char *argv[])
{
	WriteToLog();
	GlobalDebug(0);

	Init SYSINIT;
	Game TestGame;
	TestGame.Config("Test Window", 1270, 720, "res/background.png");
	Filesystem TestSprites;
	TestSprites.SetArchive("res/testsprites.zip");
	Filesystem TestFail;
	TestFail.SetArchive("res/none.zip");
	TestFail.FileCheck("none.png");
	TestFail.OpenFile("none.png");

#ifdef __linux__
	printf("Config Directory: %s\n", setConfigDir());
	printf("Data Directory: %s\n", setDataDir());
#endif

	Input InitInput;

	Player TestPlayer;
	Entity TestEnt1;
	Entity TestEnt2;
	Entity TestSheet;
	Asset Background;

	TestSheet.LoadAsset(99, "testplayerspritesheet.png");
	TestSheet.SetClip(45, 75);

	Background.LoadAsset(0, "background.png");
	Background.SetRects(20, 20, 0, 0);
	Background.DrawAsset("stretch");

	TestPlayer.LoadAsset(1, "testplayersprite_dleft.png");
	TestPlayer.SetRects(45, 75, 0, 0);
	TestPlayer.SetClip(45, 75);
	TestPlayer.DrawAsset("");

	TestEnt1.LoadAsset(2, "testplayersprite_dright.png");
	TestEnt1.SetRects(45, 75, 100, 0);
	TestEnt1.DrawAsset("");

	TestEnt2.LoadAsset(3, "testplayersprite_down.png");
	TestEnt2.SetRects(45, 75, 100, 100);
	TestEnt2.SetClip(45, 75);
	TestEnt2.DrawAsset("");

	Text Font;
	Font.SetFont(30, "res/test.ttf");
	Font.LoadText("Test test test", 255, 255, 255, 255);
	Font.DrawText();

	Audio MainAudio;
	MainAudio.CreateAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024);
	MainAudio.OpenAudioFile("KDE-Sys-Log-Out.ogg", 0);

	YML Config;
	Config.SetParseFile("public.yaml");

	GlobalTimer fps;
	fps.StartTimer();
	fps.PauseTimer();
	fps.StopTimer();
	fps.StartTimer();

	std::string Args;
	int i;
	for (i = 1; i < argc; i++) {
		if (argv[i] != NULL)
			Args = argv[1];
		if (argv[i] == NULL) {
		}
		if (Args == "--timed") {
			SDL_Delay(3000);
		} else if (Args == "--test") {
			bool quit = false;
			while (quit == false) {
				InitInput.PollEvent();
				if (((InitInput.MainEvent.type == SDL_KEYDOWN) &&
				     (InitInput.MainEvent.key.keysym.sym == SDLK_ESCAPE)) ||
				    (InitInput.MainEvent.type == SDL_QUIT)) {
					quit = true;
					if (quit == true) {
					}
				}
			}
		}
	}

	return 0;
}
