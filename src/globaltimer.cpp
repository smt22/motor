#include "globaltimer.h"
#include <SDL.h>
#include <iostream>

GlobalTimer::GlobalTimer()
{
	StartTick = 0;
	PausedTick = 0;
	Started = false;
	Paused = false;
	StartTimer();
}

GlobalTimer::~GlobalTimer()
{
	StopTimer();
}

void GlobalTimer::StartTimer()
{
	Started = true;
	Paused = false;

	StartTick = SDL_GetTicks();
	std::cout << "Timer started\n";
}

void GlobalTimer::StopTimer()
{
	Started = false;
	Paused = false;
	std::cout << "Timer stopped\n";
}

void GlobalTimer::PauseTimer()
{
	if ((Started == true) && (Paused == false)) {
		Paused = true;
		PausedTick = SDL_GetTicks() - StartTick;
	}
	std::cout << "Timer paused\n";
}

void GlobalTimer::UnpauseTimer()
{
	if (Paused == true) {
		Paused = false;
		StartTick = SDL_GetTicks() - PausedTick;
		PausedTick = 0;
	}
}

int GlobalTimer::Get_Ticks()
{
	if (Started == true) {
		if (Paused == true) {
			return PausedTick;
		} else {
			return SDL_GetTicks() - StartTick;
		}
	}
	return 0;
}

bool GlobalTimer::is_started()
{
	return Started;
}

bool GlobalTimer::is_paused()
{
	return Paused;
}
