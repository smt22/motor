#include "init.h"

Init::Init()
{
	InitSDL();
	InitSDLIMG();
	InitSDLTTF();
	InitSDLMIX();
	InitPhysFS();
}

Init::~Init()
{
	DeInitSDL();
	DeInitSDLIMG();
	DeInitSDLTTF();
	DeInitSDLMIX();
	DeInitPhysFS();
}

bool Init::InitSDL()
{
	if (SDL_Init(SDL_INIT_EVERYTHING == -1)) {
		return false;
	}
	printf("[System init started]\n");
	return true;
}

void Init::DeInitSDL()
{
	printf("[System deinit started]\n");
	SDL_Quit();
}

int Init::InitSDLIMG()
{
	IMG_Init(IMG_INIT_PNG);
	std::cout << "SDL_IMG is init\n";
	return 0;
}

void Init::DeInitSDLIMG()
{
	std::cout << "SDL_IMG is deinit\n";
	IMG_Quit();
}

int Init::InitPhysFS()
{
	PHYSFS_init(NULL);
	std::cout << "PhysFS is init\n";
	if (PHYSFS_init(NULL) != 0) {
		std::cout << "PhysFS failed to init\n";
	}
	return 0;
}

void Init::DeInitPhysFS()
{
	std::cout << "PhysFS is deinit\n";
	PHYSFS_deinit();
}

int Init::InitSDLTTF()
{
	TTF_Init();
	if (TTF_Init() == -1) {
		std::cout << "SDL_TTF failed to init\n";
	} else if (TTF_Init() == 0) {
		std::cout << "SDL_TTF is init\n";
	}
	return 0;
}

void Init::DeInitSDLTTF()
{
	TTF_Quit();
	std::cout << "SDL_TTF is deinit\n";
}

int Init::InitSDLMIX()
{
	MixerFlags = MIX_INIT_OGG | MIX_INIT_MOD;
	Mix_Init(MixerFlags);
	if (Mix_Init(MixerFlags) == -1) {
		std::cout << "SDL_MIX failed to init\n";
		return -1;
	} else if (Mix_Init(MixerFlags) != -1) {
		std::cout << "SDL_MIX is init\n";
		return 1;
	}
	return 0;
}

void Init::DeInitSDLMIX()
{
	Mix_Quit();
	std::cout << "SDL_MIX is deinit\n";
}
