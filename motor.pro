system(cd $PWD; ./bin/version-date.sh)
TARGET = MotorTest
DEPENDPATH += include src
INCLUDEPATH += include
include(motor-conf.pri)
CONFIG -= qt
g++ {
	QMAKE_CXXFLAGS += -fprofile-arcs -ftest-coverage -std=c++11
	LIBS += -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lyaml -lphysfs -lgcov
}

QMAKE_CLEAN += *.gcda *.gcno *.txt *.Debug *.Release MotorTest -r release debug

clang {
	QMAKE_CXX = clang++
	QMAKE_CC = clang
	QMAKE_CXXFLAGS += -std=c++11
	LIBS += -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lyaml -lphysfs
}

HEADERS += include/assets.h \
           include/audio.h \
	   include/desktop.h \
           include/entity.h \
           include/filesystem.h \
	   include/game.h \
           include/glob.h \
           include/globaltimer.h \
           include/init.h \
           include/input.h \
	   include/motor.h \
           include/parser.h \
           include/player.h \
           include/text.h \
	   include/version.h \
           include/window.h
SOURCES += src/assets.cpp \
           src/audio.cpp \
	   src/desktop.cpp \
           src/entity.cpp \
           src/filesystem.cpp \
	   src/game.cpp \
           src/glob.cpp \
           src/globaltimer.cpp \
           src/init.cpp \
	   src/input.cpp \
           src/parser.cpp \
           src/player.cpp \
           src/text.cpp \
	   src/version.cpp \
           src/window.cpp \
           test/timedtestbed.cpp
