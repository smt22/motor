#!/bin/bash

# This script checks the Linux distribution being used to install dependencies
# needed to work on motor. All sources are stored into '$HOME/.src'.

#-------------------------------------------------------------------------------
# Variables
#-------------------------------------------------------------------------------
SRCDIR=$HOME/.src
SDL2=SDL
SDL2IMG=SDL2_image-2.0.0.tar.gz
SDL2TTF=SDL2_ttf-2.0.12.tar.gz
SDL2MIX=SDL2_mixer-2.0.0.tar.gz
LIBYML=yaml-0.1.4.tar.gz
PHYSFS=physfs-2.0.3.tar.bz2
DIST=/etc/os-release
DEBBLD="build-essential cmake"
ARCHBLD="cmake base-devel"
RPMBLD="SDL2 SDL2_image SDL2_mixer SDL_ttf"
ARCHDEPS="sdl2 sdl2_image sdl2_ttf sdl2_mixer libyaml physfs"
RPMDEPS="SDL2*-devel* libyaml*-devel* physfs*-devel*"
DEBDEPS="build-essential cmake libsdl2-dev libsdl2-mixer-dev libsdl2-image-dev libsdl2-ttf-dev libphysfs-dev libyaml-dev"
UBTDEPS="build-essential cmake libphysfs-dev libyaml-dev"
VAR1=$1

#-------------------------------------------------------------------------------
# OS Check / Dependency install
#-------------------------------------------------------------------------------
PkgInstall () {
	if grep -c Arch $DIST &>/dev/null ; then
		sudo pacman -S $ARCHDEPS
	fi
	if grep -c Debian $DIST &>/dev/null ; then
		sudo apt-get install -y $DEBDEPS
	fi
	if grep -c Ubuntu $DIST &>/dev/null ; then
		sudo apt-get install -y $UBTDEPS
	fi
	if grep -c Fedora $DIST &>/dev/null ; then
		sudo yum install $RPMDEPS -y
	fi
}

PkgUninstall () {
	if grep -c Arch $DIST &>/dev/null ; then
		sudo pacman -R $ARCHDEPS
	fi
	if grep -c Debian $DIST &>/dev/null ; then
		sudo apt-get remove -y $DEBDEPS
	fi
	if grep -c Ubuntu $DIST &>/dev/null ; then
		sudo apt-get remove -y $UBTDEPS
	fi
	if grep -c Fedora $DIST &>/dev/null ; then
		sudo yum remove $RPMDEPS -y
	fi
}

CygwinChk() {
	if [ `uname -o` == "Cygwin" ] ; then
		# No the aren't, needs fix...
		echo "Dependencies on Cygwin are solved through default install..."
		CYGCHK="1"
		sleep 3
	else
		CYGCHK="0"
	fi
}

UbtDepCheck() {
	if grep -c Ubuntu $DIST &>/dev/null ; then
		sudo apt-get build-dep libsdl1.2 libsdl-image1.2 libsdl-ttf2.0-0 libsdl-mixer1.2 libphysfs1 -y
	fi
}

#-------------------------------------------------------------------------------
# Source download
#-------------------------------------------------------------------------------
MkDir() {
	echo "Making ~/.src directory for sources..."
	mkdir -p $SRCDIR
	cd $SRCDIR
	echo "NOTE: If the source directories are deleted the uninstall files will be lost!"
	sleep 3
}

GetPhysFS () {
	echo "Getting PhysicsFS..."
	cd $SRCDIR
	if [ ! -f "$PHYSFS" ] ; then
		echo "$PHYSFS does not exist."
		echo "Getting PhysicsFS now..."
		wget https://icculus.org/physfs/downloads/physfs-2.0.3.tar.bz2
	else
		echo "$PHYSFS tar exists..."
	fi
}

GetSDL2 () {
	echo "Pulling SDL2..."
	cd $SRCDIR
	if [ ! -f  "$SDL2" ] ; then
		echo "$SDL2 does not exist."
		echo "Pulling SDL2 now..."
		hg clone http://hg.libsdl.org/SDL SDL
	else
		echo "$SDL2 folder exists."
	fi
}

GetSDL2Mixer () {
	echo "Pulling SDL2_mixer..."
	cd $SRCDIR
	if [ ! -f "$SDL2MIX" ] ; then
		echo "$SDL2MIX does not exist."
		echo "Pulling SDL2_mixer now..."
		wget http://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.0.tar.gz
	else
		echo "$SDL2MIX exists."
	fi
}

GetSDL2Image () {
	echo "Pulling SDL2_image..."
	cd $SRCDIR
	if [ ! -f "$SDL2IMG" ] ; then
		echo "$SDL2IMG does not exist."
		echo "Pulling SDL2_image now..."
		wget http://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.0.tar.gz
	else
		echo "$SDL2IMG exists."
	fi
}

GetSDL2Ttf () {
	echo "Pulling SDL2_ttf..."
	cd $SRCDIR
	if [ ! -f "$SDL2TTF" ] ; then
		echo "$SDL2TTF does not exist."
		echo "Pulling SDL2_ttf now..."
		wget http://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.12.tar.gz
	else
		echo "$SDL2TTF exists."
	fi
}

Getlibyaml () {
	echo "Pulling libyaml.."
	if [ ! -f "$LIBYML" ] ; then
		echo "$LIBYML does not exist."
		echo "Pulling libyaml now..."
		wget pyyaml.org/download/libyaml/yaml-0.1.4.tar.gz
	else
		echo "$LIBYML exists."
	fi
}

#-------------------------------------------------------------------------------
# Library install check
#-------------------------------------------------------------------------------
SDL2Chck () {
	if [ -f "/usr/lib/libSDL2.a" ] ; then
		echo "SDL2 already installed..."
		if [ $VAR1 == '--uninstall' ] ; then
			cd $SRCDIR/SDL
			if [ $CYGCHK == "1" ] ; then
				make uninstall
			else
				sudo make uninstall
			fi
		fi
	else
		echo "SDL2 not installed..."
		GetSDL2
		cd $SRCDIR
		echo "Configuring, making, and installing SDL2..."
		cd SDL
		./configure
		make
		if [ $CYGCHK == "1" ] ; then
			make install
		else
			sudo make install
			cd $SRCDIR
		fi
	fi
}

SDL2MixChck () {
	if [ -f "/usr/lib/libSDL2_mixer.a" ]
	then
		echo "SDL2_mixer already installed..."
		if [ $VAR1 == '--uninstall' ] ; then
			cd $SRCDIR/SDL2_mixer-2.0.0
			if [ CYGCHK == "1" ] ; then
				make uninstall
			else
				sudo make uninstall
			fi
			sudo make uninstall
		fi
	else
		echo "SDL2_mixer not installed..."
		GetSDL2Mixer
		tar -xf $SDL2MIX
		echo "Configuring, making and installing SDL2_mixer..."
		cd SDL2_mixer-2.0.0
		./configure
		make
		if [ $CYGCHK == "1" ] ; then
			make install
		else
			sudo make install
			cd $SRCDIR
		fi
	fi
}

SDL2ImgChck () {
	if [ -f "/usr/lib/libSDL2_image.a" ]
	then
		echo "SDL2_image already installed.."
		if [ $VAR1 == '--uninstall' ] ; then
			cd $SRCDIR/SDL2_image-2.0.0
			if [ $CYGCHK == "1" ] ; then
				make uninstall
			else
				sudo make uninstall
			fi
			sudo make uninstall
		fi
	else
		echo "SDL2_image not installed..."
		GetSDL2Image
		tar -xf $SDL2IMG
		echo "Configuring, making and installing SDL2_image..."
		cd SDL2_image-2.0.0
		./configure
		make
		if [ $CYGCHK == "1" ] ; then
			make install
		else
			sudo make install
			cd $SRCDIR
		fi
	fi
}

SDL2TtfChck () {
	if [ -f "/usr/lib/libSDL2_ttf.a" ]
	then
		echo "SDL2_ttf already intalled..."
		if [ $VAR1 == '--uninstall' ] ; then
			cd $SRCDIR/SDL2_ttf-2.0.12
			if [ $CYGCHK == "1" ] ; then
				make uninstall
			else
				sudo make uninstall
			fi
			sudo make uninstall
		fi
	else
		echo "SDL2_ttf not installed..."
		GetSDL2Ttf
		cd $SRCDIR
		tar -xf $SDL2TTF
		echo "Configuring, making and installing SDL2_ttf..."
		cd SDL2_ttf-2.0.12
		./configure
		make
		if [ $CYGCHK == "1" ] ; then
			make install
		else
			sudo make install
			cd $SRCDIR
		fi
	fi
}

YamlChck () {
	if [ -f "/usr/lib/libyaml.a" ]
	then
		echo "libyaml already installed..."
		if [ $VAR1 == '--uninstall' ] ; then
			cd $SRCDIR/yaml-0.1.4
			if [ $CYGCHK == "1" ] ; then
				make uninstall
			else
				sudo make uninstall
			fi
			sudo make uninstall
		fi
	else
		echo "libyaml not installed..."
		Getlibyaml
		cd $SRCDIR
		tar -xf $LIBYML
		echo "Configuring, making, and installing libyaml..."
		cd yaml-0.1.4
		./configure
		make
		if [ $CYGCHK == "1" ] ; then
			make install
		else
			sudo make install
			cd $SRCDIR
		fi
	fi
}

PhysChck () {
	if [ -f "/usr/local/lib/libphysfs.a" ]
	then
		echo "PhysicsFS already installed..."
		if [ $VAR1 == '--uninstall' ] ; then
			if [ $CYGCHK == "1" ] ; then
				rm /usr/local/lib/libphysfs*
				rm /usr/local/lib/physfs.h
			else
				sudo rm /usr/local/lib/libphysfs*
				sudo rm /usr/local/include/physfs.h
			fi
		fi
	else
		echo "PhysicsFS not installed..."
		GetPhysFS
		cd $SRCDIR
		tar -xf $PHYSFS
		echo "Configuring, making and installing PhysicsFS..."
		cd physfs-2.0.3
		cmake CMakeLists.txt
		make
		if [ $CYGCHK == "1" ] ; then
			make install
		else
			sudo make install
			cd $SRCDIR
		fi
	fi
}

#-------------------------------------------------------------------------------
# Library path set/check
#-------------------------------------------------------------------------------
PathSet() {
	if [ $CYGCHK == "1" ] ; then
		echo "Using Cygwin, no ld to config c:"
	else
		echo "Setting path..."
		if grep -c '/usr/local/lib' /etc/ld.so.conf &>/dev/null
		then
			sudo ldconfig
			echo "Path already set..."
		else
			export PATH=$PATH:/usr/local/bin
			echo 'echo "/usr/local/lib" >> /etc/ld.so.conf' | sudo -s
			sudo ldconfig
		fi
	fi
}
PathUpdate() {
	sudo ldconfig
}

#-------------------------------------------------------------------------------
# Install/Uninstall Prompt
#-------------------------------------------------------------------------------
Install() {
	CygwinChk
	PkgInstall
	PathUpdate
	if [ $CYGCHK == "1" ] ; then
		SDL2Chck
		SDL2ImgChck
		SDL2TtfChck
		SDL2MixChck
		PhysChck
		YamlChck
		PathSet
		PathUpdate
	fi
}

Uninstall() {
	CygwinChk
	SDL2Chck
	SDL2ImgChck
	SDL2TtfChck
	SDL2MixChck
	PhysChck
	YamlChck
	PathSet
	PathUpdate
}

SrcIn() {
	CygwinChk
	MkDir
	UbtDepCheck
	SDL2Chck
	SDL2ImgChck
	SDL2TtfChck
	SDL2MixChck
	PhysChck
	YamlChck
	PathSet
	PathUpdate
}

usage () {
	echo "usage: motorwork [option]"
	echo
	echo "Options:"
	echo "    -i, --install		Install dependencies through native package manager"
	echo "    -u, --uninstall		Uninstall dependencies through native package manager"
	echo "    -si, --source-install	Install dependencies from source"
	echo "    -su, --source-uninstall	Uninstall dependencies installed from source"
	exit
}

case "$1" in
	'-i'|'--install')
		PkgInstall
		;;
	'-u'|'--uninstall')
		PkgUninstall
		;;
	'-si'|'--source-install')
		SrcIn
		;;
	'-su'|'--source-uninstall')
		;;
	''|'-h'|'--help')
		usage
		;;
	*)
		echo "Command not found, see motorwork --help"
		;;
esac
